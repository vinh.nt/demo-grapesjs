import "./App.css";
import React, { useState,useEffect } from "react";
import 'grapesjs/dist/css/grapes.min.css';
import grapesjs from 'grapesjs';
import myPlugin from './tabs/index'
import defOptions from './tabs/options';
import 'grapesjs-preset-webpage'
import 'grapesjs-preset-newsletter'


export default function App() {
  useEffect(() => {
    const editor = grapesjs.init({
      container: '#gjs',
      fromElement: true,
      height: '300px',
      width: '100%',
      storageManager: false,
      panels: { defaults: [] },
      plugins: [myPlugin,'gjs-preset-webpage'],
      blockManager: {
        appendTo: '#blocks',
        blocks: [
          {
            id: 'section', // id is mandatory
            label: '<b>Section</b>', // You can use HTML/SVG inside labels
            attributes: { class:'gjs-block-section' },
            content: `<section>
                        <h1>This is a simple title</h1>
                        <div>This is just a Lorem text: Lorem ipsum dolor sit amet</div>
                      </section>`,
          },
        ]
      },
    });
    editor.BlockManager.add('tabs',{
      id: 'tabs',
      label: 'Tabs',
      content: "<h1>1111111111</h1>"
    })
  }, [])

  return (
    <>
      <div id="gjs">
        <h1>Hello World Component!</h1>
      </div>
      <div id="blocks"></div>
    </>
  );
}
