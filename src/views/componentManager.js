import Block from './component/Block'

const configComponent = [
    {
        id:'my-first-block',
        label: 'Simple block',
        component : Block
    },
    {
        id:'my-first-block-2',
        label: 'Simple block-2',
        component : Block
    },
]



const myPlugin = function(editor){
    return configComponent.forEach(({id,label,component}) =>
        editor.BlockManager.add(id, {
            label,
            content: component,
            category: 'Custom Category',
        })
    )
    
}
export default myPlugin;