import 'grapesjs-preset-webpage';
import {GrapesjsReact} from 'grapesjs-react';
import plugin from 'grapesjs-table';
import pluginTabs from 'grapesjs-tabs';
import grapesjs from 'grapesjs';
import "grapesjs/dist/css/grapes.min.css";
import myPlugin from '../views/componentManager'

function GrapesJS(props) {
    return (
        <GrapesjsReact
            id='grapesjs-react'
            plugins={[
            'gjs-preset-webpage',
            'gjs-blocks-basic',
            "grapesjs-tabs",
            myPlugin,
            plugin,
            pluginTabs
            ]}
        />
    )
}

GrapesJS.propTypes = {

}

export default GrapesJS

